$("tr#book_<%= book.id %>").replaceWith("<%= j render 'data_row', book: @book %>")
$("tr#book_<%= book.id %>").fadeTo 0, 0, ->
  $(".rateit").rateit()
  $("tr#book_<%= book.id %>").fadeTo 1500, 1
# $("#message").html "<%= j render 'message' %>"
